/*
 * (c) Copyright 2021-2022 Micro Focus or one of its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import xml2js, { parseString } from 'xml2js';
import fs from 'fs-extra';
import { TEST_RESULT_FILE } from '../utils/files.js';
import format from 'dateformat';

const xmlBuilder = new xml2js.Builder();

const convertTestResultsToOctaneFormat = async (): Promise<void> => {
    const testSuite = {
        testsuite: [{}]
    };
    const xmlInfoFiles = getAllFilesFromDir(TEST_RESULT_FILE).filter(file =>
        file.endsWith('.Info.xml')
    );
    for (const file of xmlInfoFiles) {
        const xmlContent: string = fs.readFileSync(file, 'utf-8');
        const result = await parseXmlStringToObject(xmlContent);
        const octaneTestName: string = file
            .toString()
            .substring(
                file.toString().indexOf('/') + 1,
                file
                    .toString()
                    .indexOf(format(Date.now(), 'yyyy-mm-dd').toString()) - 1
            );
        const testCase = getTestCase(result, octaneTestName);
        testSuite.testsuite.push(testCase);
    }
    const resultsXml: string = xmlBuilder.buildObject(testSuite);
    fs.writeFileSync(`${TEST_RESULT_FILE}/Results.xml`, resultsXml);
};

const getAllFilesFromDir = (
    dir: string,
    allFilesList: string[] = []
): string[] => {
    const files: string[] = fs.readdirSync(dir);
    files.map(file => {
        const name: string = dir + '/' + file;
        if (fs.statSync(name).isDirectory()) {
            getAllFilesFromDir(name, allFilesList);
        } else {
            allFilesList.push(name);
        }
    });

    return allFilesList;
};

const parseXmlStringToObject = (xmlString: string): Promise<Object> => {
    return new Promise((resolve, reject) => {
        parseString(
            xmlString,
            { explicitArray: false, explicitRoot: false },
            (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            }
        );
    });
};

const getTestCase = (xmlObject: any, octaneTestName: string): Object => {
    if (xmlObject.playbackErrorString) {
        return {
            testcase: {
                $: {
                    name: octaneTestName,
                    classname: ''
                },
                error: {
                    $: { message: xmlObject.playbackErrorString }
                }
            }
        };
    }
    return {
        testcase: {
            $: {
                name: octaneTestName,
                classname: ''
            }
        }
    };
};

convertTestResultsToOctaneFormat()
    .then(() =>
        console.log(
            'Silk Test Workbench results were successfully converted to Octane expected format'
        )
    )
    .catch(err => console.error(err.message, err));
